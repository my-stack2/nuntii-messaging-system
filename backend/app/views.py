from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .models import User, Chat, Message
from .serializers import UserSerializer, ChatSerializer, MessageSerializer
from django.db import connection, reset_queries

class UserView(ListCreateAPIView):
    queryset = User.objects.all().prefetch_related('chats')
    serializer_class = UserSerializer
    
class ChatView(ListCreateAPIView):
    queryset = Chat.objects.all().prefetch_related("users")
    serializer_class = ChatSerializer
    
class MessageView(ListCreateAPIView):
    q = len(connection.queries)
    queryset = Message.objects.all().select_related("chat", "user")
    serializer_class = MessageSerializer

class UserDeleteRetrieveUpdateView(RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all().prefetch_related('chats')
    serializer_class = UserSerializer
    
class ChatDeleteRetrieveUpdateView(RetrieveUpdateDestroyAPIView):
    queryset = Chat.objects.all().prefetch_related("users")
    serializer_class = ChatSerializer
    
class MessageDeleteRetrieveUpdateView(RetrieveUpdateDestroyAPIView):
    q = len(connection.queries)
    queryset = Message.objects.all().select_related("chat", "user")
    serializer_class = MessageSerializer

