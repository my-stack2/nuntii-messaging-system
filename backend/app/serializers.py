from rest_framework import serializers
from .models import User, Chat, Message

class UserSerializer(serializers.ModelSerializer):
    class Meta:
            depth=1
            
            model = User
            fields = '__all__'
            
            extra_kwargs = {'password': {'write_only': True}}

class ChatSerializer(serializers.ModelSerializer):
    class Meta:
            depth=1
        
            model = Chat
            fields = '__all__'
    
class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
    