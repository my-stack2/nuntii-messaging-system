# urls.py
from django.urls import path
from .views import UserView, ChatView, MessageView, UserDeleteRetrieveUpdateView, MessageDeleteRetrieveUpdateView, ChatDeleteRetrieveUpdateView


urlpatterns = [
    path('user/', UserView.as_view(), name='user'),
    path('chat/', ChatView.as_view(), name='chat'),
    path('message/', MessageView.as_view(), name='message'),
    path('user/<uuid:pk>/', UserDeleteRetrieveUpdateView.as_view(), name='userDRU'),
    path('chat/<uuid:pk>/', ChatDeleteRetrieveUpdateView.as_view(), name='chatDRU'),
    path('message/<uuid:pk>/', MessageDeleteRetrieveUpdateView.as_view(), name='messageDRU')

]