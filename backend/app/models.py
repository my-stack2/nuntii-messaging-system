from django.db import models
from django.contrib.auth.hashers import make_password
from django.db.models.signals import pre_save
from django.dispatch import receiver
import uuid

class User(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, auto_created=True, editable=False)
    username = models.CharField(max_length=40,null=False, blank=False, unique=True)
    email = models.EmailField(max_length=255, null=False, blank=False, unique=True)
    password = models.CharField(max_length=255, null=False, blank=False)
    name = models.CharField(max_length=255,null=False, blank=False, unique=True)
    created_at = models.DateField(auto_now=True)
    
    class Meta:
        verbose_name = ("user")
        verbose_name_plural = ("users")

    def __str__(self):
        return self.name
    
    
    
class Chat(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, auto_created=True, editable=False)
    name = models.CharField(max_length=255,null=False, blank=False, unique=True)
    created_at = models.DateField(auto_now=True)
    users = models.ManyToManyField(User, related_name='chats')
    
    class Meta:
        verbose_name = ("chat")
        verbose_name_plural = ("chats")

    def __str__(self):
        return self.name
    
class Message(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, auto_created=True, editable=False)
    content = models.TextField(max_length=1000, null=False, blank=True)
    send_at = models.DateField(auto_now=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.content

@receiver(pre_save, sender=User)
def hash_password(sender, instance, **kwargs):
    instance.password = make_password(instance.password)